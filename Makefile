export COMPOSE_PROJECT_NAME ?= $(shell basename ${PWD})
export COMPOSE_FILE ?= docker-compose.yml
export UID ?= $(shell id -u)
export GID ?= $(shell id -g)

.PHONY: clean-cli
clean-cli:
	rm -rf ./src/cmd/ttab-cli/bin/*

.PHONY: build-cli
build-cli: clean-cli
	GOOS=linux go build -a -o ./src/cmd/ttab-cli/bin/ttab-cli ./src/cmd/ttab-cli