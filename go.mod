module bitbucket.org/denis_alekseev/ttab

go 1.13

require (
	github.com/alecthomas/kong v0.2.12
	github.com/sirupsen/logrus v1.7.0
)
