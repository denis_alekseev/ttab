package cmd

type Globals struct {
	Config   string `help:"Location of client config files" default:"~/config.json" type:"path"`
	Debug    bool   `short:"D" help:"Enable debug mode"`
	LogLevel string `short:"l" help:"Set the logging level (debug|info|warn|error|fatal)" default:"info"`
}
