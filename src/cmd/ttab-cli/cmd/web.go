package cmd

import (
	"bitbucket.org/denis_alekseev/ttab/src/cmd/ttab-cli/config"
	"fmt"
)

type WebCmd struct {
	DB   config.Database `embed`
	Port string          `help:"Port"`
}

func (w *WebCmd) Run(globals *Globals) error {
	fmt.Println("I'm a live")
	return nil
}
