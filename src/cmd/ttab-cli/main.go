package main

import (
	"bitbucket.org/denis_alekseev/ttab/src/cmd/ttab-cli/cmd"
	"github.com/alecthomas/kong"
)

type CLI struct {
	cmd.Globals

	Web cmd.WebCmd `cmd help:"Run web server"`
}

func main() {
	cli := CLI{}

	ctx := kong.Parse(&cli,
		kong.Name("ttab"),
		kong.Description("Web API for your timetables"),
		kong.UsageOnError(),
		kong.ConfigureHelp(kong.HelpOptions{
			Compact: true,
		}),
	)
	err := ctx.Run(&cli.Globals)
	ctx.FatalIfErrorf(err)
}
