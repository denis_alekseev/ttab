package config

type Config struct {
	Log      Log
	WebCfg   WebCfg
	Database Database
}

func (c Config) Logger() Log {
	return c.Log
}

func (c Config) Web() WebCfg {
	return c.WebCfg
}

func (c Config) DB() Database {
	return c.Database
}
