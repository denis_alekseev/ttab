package config

import (
	"github.com/sirupsen/logrus"
)

// Log конфигурация логирования
type Log struct {
	Level  string
	Format string
}

func InitLogger(cfg Log) *logrus.Logger {
	log := logrus.New()
	// форматтер и уровень сделать в зависимости от параметров конфигурации
	log.SetLevel(logrus.WarnLevel)
	log.SetFormatter(&logrus.JSONFormatter{})
	return log
}
